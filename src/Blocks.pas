unit Blocks;

interface

  const (* Block id *)
    none = 0;
    dirt = 1;
    grass = 2;
    stone = 3;
    oakWoodPlanks = 4;
    cobblestone = 5;
    bedrock = 6;
    sand = 7;
    gravel = 8;
    oakWood = 9;
    obsidian = 10;
    bookshelf = 11;
    mossStone = 12;
    blockOfIron = 13;
    blockOfGold = 14;
    blockOfDiamond = 15;
    goldOre = 16;
    ironOre = 17;
    coalOre = 18;
    diamondOre = 19;
    redstoneOre = 20;
    oakLeaves = 21;
    redFlower = 22;
    yellowFlower = 23;
    redMooshroom = 24;
    brownMooshroom = 25;
    torch = 26;
    tnt = 27;
    chest = 28;
    craftingTable = 29;
    cactus = 30;
    glass = 31;
    wool1 = 32;
    wool2 = 33;
    wool3 = 34;
    wool4 = 35;
    wool5 = 36;
    wool6 = 37;
    wool7 = 38;
    wool8 = 39;
    wool9 = 40;
    wool10 = 41;
    wool11 = 42;
    wool12 = 43;
    wool13 = 44;
    wool14 = 45;
    wool15 = 46;
    wool16 = 47;
    snow = 48;
    ladder = 49;
    water = 50;
    lava = 51;
    oakSapling = 52;
    sponge = 53;
    lapisLazuliOre = 54;
    lapisLazuliBlock = 55;
    sandstone = 56;
    tallGrass = 57;
    deadBush = 58;
    cobweb = 59;
    bricks = 60;
    snowBlock = 61;
    ice = 62;
    snowLayer = 63;
    clayBlock = 64;
    sugarCane = 65;
    pumpkin = 66;
    jackLantern = 67;
    stoneBricks = 68;
    mossyStoneBricks = 69;
    crackedStoneBricks = 70;
    chiseledStokeBricks = 71;
    ironBras = 72;
    melonBlock = 73;
    mycelium = 74;
    backgroundOakWood = 75;
    spawner = 76;
    bed1 = 77;
    bed2 = 78;
    openWoodenDoor1 = 79;
    openWoodenDoor2 = 80;
    closedWoodenDoor1 = 81;
    closedWoodenDoor2 = 82;
    birchWood = 83;
    backgroundBirchWood = 84;
    spruceWood = 85;
    backgroundSpruceWood = 86;
    spruceLeaves = 87;
    redMushroomBlock1 = 88;
    redMushroomBlock2 = 89;
    brownMushroomBlock1 = 90;
    brownMushroomBlock2 = 91;
    oakFence = 92;
    backgroundOakFence = 93;
    backgroundOakWoodPlanks = 94;
    painting1 = 95;
    painting2 = 96;
    painting3 = 97;
    painting4 = 98;
    painting5 = 99;
    painting6 = 100;
    painting7 = 101;
    giftChest = 102;
    vines = 103;
    sign = 104;
    redstoneTorch = 105;
    furnace = 106;
    closedWoodenTrapdoor = 107;
    openWoodenTrapdoor = 108;
    netherrack = 109;
    netherPortal = 110;
    glowstone = 111;
    birchLeaves = 112;
    soulSand = 113;
    birchSapling = 114;
    spruceSapling = 115;
    redstoneLampOff = 116;
    redstoneLampOn = 117;
    backgroundObsidian = 118;
    glassPlane = 119;
    farmland = 120;
    cake = 121;
    wheat = 122;
    melonStem = 123;
    pumpkinStem = 124;
    burningFurnace = 125;
    reservedBlock = 126;

  function GetTexture(id : integer) : integer;
  function GetResistant(id : integer) : integer;
  function GetTool(id : integer) : integer;
  function GetLevel(id : integer) : integer;
  function GetLightAbsorbtion(id : integer) : integer;
  function GetLightEmission(id : integer) : integer;

  function IsTransporent(id : integer) : boolean;
  function IsForeground(id : integer) : boolean;
  function IsOverlapped(id : integer) : boolean;
  function IsSolid(id : integer) : boolean;

implementation

  uses items_store;

  const
    lastBlock = 125;

  var
    bltex, hp, tool, lvl, abslight, emmlight, flags : array [0..lastBlock] of integer;

  function GetTexture(id : integer) : integer;
    begin
      result := bltex[id];
    end;

  function GetResistant(id : integer) : integer;
    begin
      result := hp[id];
    end;

  function GetTool(id : integer) : integer;
    begin
      result := tool[id];
    end;

  function GetLevel(id : integer) : integer;
    begin
      result := lvl[id];
    end;

  function GetLightAbsorbtion(id : integer) : integer;
    begin
      result := abslight[id];
    end;

  function GetLightEmission(id : integer) : integer;
    begin
      result := emmlight[id];
    end;

  function IsTransporent(id : integer) : boolean;
    begin
      result := (flags[id] and (1 << 0)) <> 0;
    end;

  function IsForeground(id : integer) : boolean;
    begin
      result := (flags[id] and (1 << 1)) <> 0;
    end;

  function IsOverlapped(id : integer) : boolean;
    begin
      result := (flags[id] and (1 << 2)) <> 0;
    end;

  function IsSolid(id : integer) : boolean;
    begin
      result := (flags[id] and (1 << 3)) <> 0;
    end;

  procedure InitBlock(id, xtex, xhp, xtool, xlvl, xabslight, xemmlight : integer; solid, transporent, foreground, overlap : boolean);
    begin
      Assert((id >= 0) and (id <= lastBlock));
      bltex[id] := xtex;
      hp[id] := xhp;
      tool[id] := xtool;
      lvl[id] := xlvl;
      abslight[id] := xabslight;
      emmlight[id] := xemmlight;
      flags[id] := 0;
      if transporent then flags[id] := flags[id] or (1 << 0);
      if foreground then flags[id] := flags[id] or (1 << 1);
      if overlap then flags[id] := flags[id] or (1 << 2);
      if solid then flags[id] := flags[id] or (1 << 3);
    end;

initialization
  InitBlock(none, 0, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(dirt, 1, 9, 2, 0, 15, 0, true, false, false, false);
  InitBlock(grass, 2, 9, 2, 0, 15, 0, true, false, false, false);
  InitBlock(stone, 3, 60, 1, 1, 15, 0, true, false, false, false);
  InitBlock(oakWoodPlanks, 4, 30, 3, 0, 15, 0, true, false, false, false);
  InitBlock(cobblestone, 5, 60, 1, 1, 15, 0, true, false, false, false);
  InitBlock(bedrock, 6, 2147483647, 0, 0, 15, 0, true, false, false, false);
  InitBlock(sand, 7, 9, 2, 0, 15, 0, true, false, false, false);
  InitBlock(gravel, 8, 9, 2, 0, 15, 0, true, false, false, false);
  InitBlock(oakWood, 9, 30, 3, 0, 15, 0, true, false, false, false);
  InitBlock(obsidian, 10, 1000, 1, 5, 15, 0, true, false, false, false);
  InitBlock(bookshelf, 11, 24, 3, 0, 0, 0, false, false, false, false);
  InitBlock(mossStone, 12, 40, 1, 1, 15, 0, true, false, false, false);
  InitBlock(blockOfIron, 13, 40, 1, 2, 15, 0, true, false, false, false);
  InitBlock(blockOfGold, 14, 60, 1, 3, 15, 0, true, false, false, false);
  InitBlock(blockOfDiamond, 15, 60, 1, 3, 15, 0, true, false, false, false);
  InitBlock(goldOre, 16, 65, 1, 3, 15, 0, true, false, false, false);
  InitBlock(ironOre, 17, 65, 1, 2, 15, 0, true, false, false, false);
  InitBlock(coalOre, 18, 65, 1, 1, 15, 0, true, false, false, false);
  InitBlock(diamondOre, 19, 65, 1, 3, 15, 0, true, false, false, false);
  InitBlock(redstoneOre, 20, 65, 1, 3, 15, 6, true, false, false, false);
  InitBlock(oakLeaves, 21, 3, 4, 1, 1, 0, true, false, false, false);
  InitBlock(redFlower, 22, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(yellowFlower, 23, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(redMooshroom, 24, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(brownMooshroom, 25, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(torch, 26, 0, 0, 0, 0, 14, false, true, false, false);
  InitBlock(tnt, 27, 0, 0, 0, 15, 0, true, false, false, false);
  InitBlock(chest, 28, 39, 3, 0, 0, 0, false, false, false, false);
  InitBlock(craftingTable, 29, 39, 3, 0, 0, 0, false, false, false, false);
  InitBlock(cactus, 30, 6, 0, 0, 0, 0, true, true, false, false);
  InitBlock(glass, 31, 6, 0, 1, 1, 0, true, true, false, false);
  InitBlock(wool1, 32, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool2, 33, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool3, 34, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool4, 35, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool5, 36, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool6, 37, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool7, 38, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool8, 39, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool9, 40, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool10, 41, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool11, 42, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool12, 43, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool13, 44, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool14, 45, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool15, 46, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(wool16, 47, 12, 0, 0, 0, 0, false, false, false, false);
  InitBlock(snow, 48, 9, 2, 0, 15, 0, true, false, false, false);
  InitBlock(ladder, 49, 6, 0, 0, 0, 0, false, true, false, false);
  InitBlock(water, 50, 2147483647, 0, 0, 1, 0, false, true, true, true);
  InitBlock(lava, 55, 2147483647, 0, 0, 15, 15, false, true, true, true);
  InitBlock(oakSapling, 60, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(sponge, 61, 9, 0, 0, 15, 0, true, false, false, false);
  InitBlock(lapisLazuliOre, 62, 65, 1, 2, 15, 0, true, false, false, false);
  InitBlock(lapisLazuliBlock, 63, 65, 1, 2, 15, 0, true, false, false, false);
  InitBlock(sandstone, 64, 50, 1, 1, 15, 0, true, false, false, false);
  InitBlock(tallGrass, 65, 0, 4, 1, 0, 0, false, true, false, true);
  InitBlock(deadBush, 66, 0, 4, 1, 0, 0, false, true, false, true);
  InitBlock(cobweb, 67, 60, 4, 1, 0, 0, false, true, false, true);
  InitBlock(bricks, 68, 60, 1, 1, 15, 0, true, false, false, false);
  InitBlock(snowBlock, 69, 3, 2, 0, 15, 0, true, false, false, false);
  InitBlock(ice, 70, 8, 1, 6, 2, 0, true, true, false, false);
  InitBlock(snowLayer, 71, 2, 2, 1, 1, 0, false, true, true, true);
  InitBlock(clayBlock, 72, 9, 2, 0, 15, 0, true, false, false, false);
  InitBlock(sugarCane, 73, 0, 0, 0, 0, 0, false, true, true, true);
  InitBlock(pumpkin, 74, 15, 3, 0, 1, 0, true, false, false, false);
  InitBlock(jackLantern, 75, 15, 3, 0, 1, 15, true, false, false, false);
  InitBlock(stoneBricks, 76, 60, 1, 1, 15, 0, true, false, false, false);
  InitBlock(mossyStoneBricks, 77, 60, 1, 1, 15, 0, true, false, false, false);
  InitBlock(crackedStoneBricks, 78, 60, 1, 1, 15, 0, true, false, false, false);
  InitBlock(chiseledStokeBricks, 79, 60, 1, 1, 15, 0, true, false, false, false);
  InitBlock(ironBras, 80, 60, 1, 1, 0, 0, false, true, true, false);
  InitBlock(melonBlock, 81, 15, 0, 0, 15, 0, true, false, false, false);
  InitBlock(mycelium, 82, 8, 2, 0, 15, 0, true, false, false, false);
  InitBlock(backgroundOakWood, 98, 30, 3, 0, 0, 0, false, false, false, false);
  InitBlock(spawner, 83, 75, 0, 0, 0, 0, false, true, false, false);
  InitBlock(bed1, 84, 3, 0, 0, 0, 0, false, true, false, false);
  InitBlock(bed2, 85, 3, 0, 0, 0, 0, false, true, false, false);
  InitBlock(openWoodenDoor1, 86, 30, 3, 0, 0, 0, false, true, false, false);
  InitBlock(openWoodenDoor2, 102, 30, 3, 0, 0, 0, false, true, false, false);
  InitBlock(closedWoodenDoor1, 88, 30, 3, 0, 0, 0, true, true, true, false);
  InitBlock(closedWoodenDoor2, 104, 30, 3, 0, 0, 0, true, true, true, false);
  InitBlock(birchWood, 90, 30, 3, 0, 0, 0, true, false, false, false);
  InitBlock(backgroundBirchWood, 99, 30, 3, 0, 0, 0, false, false, false, false);
  InitBlock(spruceWood, 91, 30, 3, 0, 0, 0, true, false, false, false);
  InitBlock(backgroundSpruceWood, 100, 30, 3, 0, 0, 0, false, false, false, false);
  InitBlock(spruceLeaves, 92, 3, 4, 1, 1, 0, true, false, false, false);
  InitBlock(redMushroomBlock1, 93, 3, 3, 0, 2, 0, true, false, false, false);
  InitBlock(redMushroomBlock2, 95, 3, 3, 0, 0, 0, false, false, false, false);
  InitBlock(brownMushroomBlock1, 94, 3, 3, 0, 2, 0, true, false, false, false);
  InitBlock(brownMushroomBlock2, 95, 3, 3, 0, 0, 0, false, false, false, false);
  InitBlock(oakFence, 96, 30, 3, 0, 0, 0, true, true, false, false);
  InitBlock(backgroundOakFence, 101, 30, 3, 0, 0, 0, false, true, false, false);
  InitBlock(backgroundOakWoodPlanks, 97, 30, 3, 0, 0, 0, false, false, false, false);
  InitBlock(painting1, 106, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(painting2, 107, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(painting3, 108, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(painting4, 109, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(painting5, 110, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(painting6, 111, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(painting7, 112, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(giftChest, 113, 30, 3, 0, 0, 15, false, false, false, false);
  InitBlock(vines, 114, 3, 4, 1, 0, 0, false, true, true, true);
  InitBlock(sign, 115, 15, 0, 0, 0, 0, false, true, false, false);
  InitBlock(redstoneTorch, 116, 0, 0, 0, 0, 7, false, true, false, true);
  InitBlock(furnace, 117, 40, 3, 0, 0, 0, false, false, false, false);
  InitBlock(closedWoodenTrapdoor, 118, 30, 3, 0, 0, 0, true, true, true, false);
  InitBlock(openWoodenTrapdoor, 119, 30, 3, 0, 0, 0, false, true, false, false);
  InitBlock(netherrack, 120, 30, 1, 1, 15, 0, true, false, false, false);
  InitBlock(netherPortal, 129, 2147483647, 0, 0, 0, 11, false, true, true, false);
  InitBlock(glowstone, 121, 10, 1, 0, 0, 15, true, false, false, false);
  InitBlock(birchLeaves, 135, 3, 4, 1, 1, 0, true, false, false, false);
  InitBlock(soulSand, 122, 9, 2, 0, 15, 0, true, false, false, false);
  InitBlock(birchSapling, 123, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(spruceSapling, 124, 0, 0, 0, 0, 0, false, true, false, true);
  InitBlock(redstoneLampOff, 125, 10, 1, 0, 0, 0, true, false, false, false);
  InitBlock(redstoneLampOn, 126, 10, 1, 0, 0, 15, true, false, false, false);
  InitBlock(backgroundObsidian, 127, 1000, 1, 5, 15, 0, false, false, false, false);
  InitBlock(glassPlane, 128, 6, 0, 1, 1, 0, false, true, false, false);
  InitBlock(farmland, 136, 9, 2, 0, 15, 0, true, false, false, false);
  InitBlock(cake, 137, 9, 0, 0, 0, 0, false, true, false, false);
  InitBlock(wheat, 138, 1, 0, 0, 0, 0, false, true, false, true);
  InitBlock(melonStem, 139, 1, 0, 0, 0, 0, false, true, false, true);
  InitBlock(pumpkinStem, 139, 1, 0, 0, 0, 0, false, true, false, true);
  InitBlock(burningFurnace, 134, 40, 3, 0, 0, 15, false, false, false, false);
end.
