unit Tools;

interface

  const
    pickaxe1 = 0;
    pickaxe2 = 1;
    pickaxe3 = 2;
    pickaxe4 = 3;
    pickaxe5 = 4;
    shovel1 = 5;
    shovel2 = 6;
    shovel3 = 7;
    shovel4 = 8;
    shovel5 = 9;
    axe1 = 10;
    axe2 = 11;
    axe3 = 12;
    axe4 = 13;
    axe5 = 14;
    shears1 = 15;
    sword1 = 16;
    sword2 = 17;
    sword3 = 18;
    sword4 = 19;
    sword5 = 20;
    rod1 = 21;
    lighter1 = 22;
    hoe1 = 23;
    hoe2 = 24;
    hoe3 = 25;
    hoe4 = 26;
    hoe5 = 27;

  const
    pickaxe = 0;
    shovel = 1;
    axe = 2;
    shears = 3;
    sword = 4;
    rod = 5;
    lighter = 6;
    hoe = 7;

  function GetType(id : integer) : integer;
  function GetLevel(id : integer) : integer;
  function GetSpeed(id : integer) : integer;
  function GetDamage(id : integer) : integer;

implementation

  const
    lastTool = 27;

  var
    typ, lvl, speed, damage : array [0..lastTool] of integer;

  function GetType(id : integer) : integer;
    begin
      result := typ[id];
    end;

  function GetLevel(id : integer) : integer;
    begin
      result := lvl[id];
    end;

  function GetSpeed(id : integer) : integer;
    begin
      result := speed[id];
    end;

  function GetDamage(id : integer) : integer;
    begin
      result := damage[id];
    end;

  procedure InitTool(id, xtyp, level, xspeed, xdamage : integer);
    begin
      typ[id] := xtyp;
      lvl[id] := level;
      speed[id] := xspeed;
      damage[id] := xdamage;
    end;

initialization
  InitTool(pickaxe1, pickaxe, 1, 2, 1);
  InitTool(pickaxe2, pickaxe, 2, 3, 1);
  InitTool(pickaxe3, pickaxe, 3, 4, 1);
  InitTool(pickaxe4, pickaxe, 4, 6, 1);
  InitTool(pickaxe5, pickaxe, 5, 6, 1);
  InitTool(shovel1, shovel, 1, 2, 1);
  InitTool(shovel2, shovel, 2, 3, 1);
  InitTool(shovel3, shovel, 3, 4, 1);
  InitTool(shovel4, shovel, 4, 6, 1);
  InitTool(shovel5, shovel, 5, 6, 1);
  InitTool(axe1, axe, 1, 2, 1);
  InitTool(axe2, axe, 2, 3, 1);
  InitTool(axe3, axe, 3, 4, 1);
  InitTool(axe4, axe, 4, 6, 1);
  InitTool(axe5, axe, 5, 6, 1);
  InitTool(shears1, shears, 1, 5, 1);
  InitTool(sword1, sword, 1, 2, 4);
  InitTool(sword2, sword, 2, 3, 5);
  InitTool(sword3, sword, 3, 4, 6);
  InitTool(sword4, sword, 4, 6, 7);
  InitTool(sword5, sword, 5, 6, 7);
  InitTool(rod1, rod, 1, 1, 1);
  InitTool(lighter1, lighter, 1, 1, 1);
  InitTool(hoe1, hoe, 1, 2, 1);
  InitTool(hoe2, hoe, 2, 3, 1);
  InitTool(hoe3, hoe, 3, 4, 1);
  InitTool(hoe4, hoe, 4, 6, 1);
  InitTool(hoe5, hoe, 5, 6, 1);
end.
