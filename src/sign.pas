unit sign;

interface
 var
  b_sign:array [0..31] of boolean;
  none0:boolean;
  t_sign:array [0..31] of string;
  none1:boolean;

  function createsign(ii,ij:integer):integer;
  procedure destsign(id:integer);

implementation
uses maps;

function createsign(ii,ij:integer):integer;
 var
  ix,ymp:integer;
  tx:array [0..3] of integer;
  exitCmd,cli:command;
 begin
  for ix:=0 to 31 do
   if b_sign[ix]=false then
    begin
     b_sign[ix]:=true;
     t_sign[ix]:='';
     setmapinfo(ix,ii,ij);
     debug('Sg'+ix);

     clearForm;
     exitCmd:=createCommand('Ok',CM_OK,1);
     showForm;
     addCommand(exitCmd);
     ymp:=formAddString('Text:');
     tx[0]:=formAddTextField('','',15,TF_ANY);
     tx[1]:=formAddTextField('','',15,TF_ANY);
     tx[2]:=formAddTextField('','',15,TF_ANY);
     tx[3]:=formAddTextField('','',15,TF_ANY);
     repaint;
     delay(100);
     repeat
      cli:=getClickedCommand;
     until cli=exitCmd;
     t_sign[ix]:=formGetText(tx[0])+#13+formGetText(tx[1])+#13+formGetText(tx[2])+#13+formGetText(tx[3])+#13;
     showCanvas;

     createsign:=0;
     exit;
    end; else
   createsign:=-1;
 end;

procedure destsign(id:integer);
 var
  ix:integer;
 begin
  b_sign[id]:=false;
  t_sign[id]:='';
 end;

initialization

end.
