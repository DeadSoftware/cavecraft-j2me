unit ItemsLogic;

interface

  procedure DrawSmall(id, x, y : integer);
  procedure Draw(id, sum, x, y : integer; drawIndicator : boolean);

implementation

  uses Items, Vars, Func;

  procedure DrawSmall(id, x, y : integer);
    var
      texid, source : integer;
    begin
      texid := Items.GetTexture(id);
      source := Items.GetTextureSource(id);
      if source = 0 then DrawImage(Vars.tex8[texid], x, y)
      else if source = 1 then DrawImage(Vars.item8[texid], x, y)
    end;

  procedure Draw(id, sum, x, y : integer; drawIndicator : boolean);
    const
      maxLength = 15;
      maxHeight = 14;
      redLength = 3;
    var
      texid, source, class, max, len : integer;
    begin
      if sum <= 0 then exit;

      texid := Items.GetTexture(id);
      source := Items.GetTextureSource(id);
      if source = 0 then DrawImage(Vars.tex[texid], x, y)
      else if source = 1 then DrawImage(Vars.item[texid], x, y)

      if drawIndicator and (sum > 1) then begin
        class := Items.GetIndicatorType(id);
        if class = numeric then begin
          Func.DrawFontText('' + sum, x, y + 8)
        end else if class = line then begin
          max := Items.GetMaximum(id);
          len := sum * maxLength / max;
          if len <= redLength then SetColor(255, 0, 0) else SetColor(0, 255, 0);
          DrawLine(x, y + maxHeight, x + len, y + maxHeight);
        end;
      end;
    end;

initialization

end.
