unit Particles;

interface

  const
    none = 0;
    whiteSmoke = 1;
    graySmoke = 2;
    blackSmoke = 3;
    redSmoke = 4;
    pinkSmoke = 5;
    bubble = 6;
    explosion = 7;

  var
    enabled : boolean;

  procedure Create(typ, x, y : integer);
  procedure Update;
  procedure Draw(camx, camy : integer);

  procedure LoadTextures(path : string);
  procedure FreeTextures;

  procedure SaveData;
  procedure LoadData;
  procedure ResetData;

implementation

  uses vars, maps, func, canvas, jsr75i;

  const
    lastParticle = 64;

  var
    tail : integer;
    ptype, px, py, pa : array [0..lastParticle] of integer;
    smokeSprites : array [0..4, 0..7] of image;
    explosionSprites : array [0..15] of image;
    bubbleSprite : image;

  procedure Create(typ, x, y : integer);
    begin
      if not enabled then exit;
      ptype[tail] := typ;
      px[tail] := x;
      py[tail] := y;
      pa[tail] := 0;
      tail := (tail + 1) mod lastParticle;
    end;

  procedure Update;
    var
      typ, i : integer;
    begin
      if not enabled then exit;
      for i := 0 to lastParticle do if ptype[i] <> none then begin
        typ := ptype[i];
        if (typ >= whiteSmoke) and (typ <= pinkSmoke) then begin
          py[i] := py[i] - 1;
          pa[i] := pa[i] + 1;
          if pa[i] > 14 then ptype[i] := none;
        end else if typ = bubble then begin
          py[i] := py[i] - 1;
          if GetMap(px[i] / 16, py[i] / 16) <> 50 then ptype[i] := none;
        end else if typ = explosion then begin
          pa[i] := pa[i] + 1;
          if pa[i] > 15 then ptype[i] := none;
        end;
      end;
    end;

  procedure Draw(camx, camy : integer);
    var
      typ, i : integer;
    begin
      if not enabled then exit;
      for i := 0 to lastParticle do if ptype[i] <> none then begin
        typ := ptype[i];
        if (typ >= whiteSmoke) and (typ <= pinkSmoke) then begin
          DrawImage(smokeSprites[typ - 1, pa[i] / 2], px[i] - camx, py[i] - camy);
        end else if typ = bubble then begin
          DrawImage(bubbleSprite, px[i] - camx, py[i] - camy)
        end else if typ = explosion then begin
          DrawImage(explosionSprites[pa[i]], px[i] - camx, py[i] - camy);
        end;
      end;
    end;

  procedure LoadTextures(path : string);
    var
      im : image;
      i, j : integer;
    begin
      if not enabled then exit;
      im := ld_tex('particles.png', path, 'terrain/');
      for i := 0 to 4 do begin
        for j := 0 to 7 do begin
          smokeSprites[i, j] := rotate_image_from_image(im, 8 * j, 8 * i, 8, 8, 0);
        end;
      end;

      bubbleSprite := rotate_image_from_image(im, 0, 40, 8, 8, 0);

      im := ld_tex('explosion.png', path, 'terrain/');
      for i := 0 to 15 do begin
        explosionSprites[i] := rotate_image_from_image(im, 32 * i, 0, 32, 32, 0);      
      end;
    end;

  procedure FreeTextures;
    var
      nullimg : image;
      i, j : integer;
    begin
      for i := 0 to 4 do begin
        for j := 0 to 7 do begin
          smokeSprites[i, j] := nullimg;
        end;
      end;

      bubbleSprite := nullimg;

      for i := 0 to 15 do begin
        explosionSprites[i] := nullimg;
      end;
    end;

  procedure SaveData;
    var
      i : integer;
    begin
      for i := 0 to lastParticle do begin
        write_byte(ptype[i]);
        WriteInt(px[i]);
        WriteInt(py[i]);
        WriteInt(pa[i]);
      end;
    end;

  procedure LoadData;
    var
      i : integer;
    begin
      for i := 0 to lastParticle do begin
        ptype[i] := read_byte;
        px[i] := ReadInt;
        py[i] := ReadInt;
        pa[i] := ReadInt;
      end;
    end;

  procedure ResetData;
    var
      i : integer;
    begin
      for i := 0 to lastParticle do begin
        ptype[i] := none;
        px[i] := 0;
        py[i] := 0;
        pa[i] := 0;
      end;
    end;

end.
