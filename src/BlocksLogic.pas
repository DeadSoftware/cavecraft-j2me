unit BlocksLogic;

interface

  procedure Draw(x, y, camx, camy : integer);

implementation

  uses Vars, Blocks, Maps;

  procedure Draw(x, y, camx, camy : integer);
    const
      tileSize = 16;
    var
      id : integer;
      texid : integer;
    begin
      id := Maps.GetMap(x, y);
      texid := Blocks.GetTexture(id);
      DrawImage(tex[texid], x * tileSize - camx, y * tileSize - camy);
    end;

initialization

end.
