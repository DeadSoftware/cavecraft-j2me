#!/bin/sh

set -e

NAM=CaveCraft
VER=1.9.0
VEN=DeaDSoftWare
URL=https://deadsoftware.ru
ICO=/title/icon.png

# MPC="/home/deaddoomer/work/mp3CC-linux/mpc/mp3CC"
# MPS="/home/deaddoomer/work/mp3CC-linux/mps/stubs"
# MPL="/home/deaddoomer/work/mp3CC-linux/mpl"

MPC=mp3CC
MPS=/usr/share/midletpascal/stubs
# MPL=/usr/share/midletpascal/libs
MPL=libs
LIBDIR=/usr/share/java/microemulator/lib

OBJ=classes
LIB=libs
RES=res
SRC=src

SOURCES=`find $SRC -name '*.mpsrc' -or -name '*.pas'`

mp3cc() {
	$MPC -s "$1" -o "$OBJ" -l "$MPL" -p "$LIB" -m1 -c1 $2 | grep -v "@\|\^"
}

printJarClass() {
	find "$1" -name '*.class' -printf " -C $1 %f"
}

makelibs() {
      javac -Xlint:-options -d $LIB \
            -classpath `find $LIBDIR -type f -name '*.jar' -printf '%p:'` \
            -source 1.3 -target 1.1 `find libs -type f -name '*.java'`
}

rm -rf $OBJ
mkdir -p $OBJ

echo "===> Make libs"
makelibs

echo "===> Generate symbols"

for I in $SOURCES
do
	mp3cc "$I" -d
done

echo "===> Compile sources"

for I in $SOURCES
do
	mp3cc "$I"
done

mkdir -p $OBJ/META-INF
cat > $OBJ/META-INF/MANIFEST.MF <<!
Manifest-Version: 1.0
MIDlet-1: $NAM, $ICO, FW
MIDlet-Name: $NAM
MIDlet-Vendor: $VEN
MIDlet-Version: $VER
MicroEdition-Configuration: CLDC-1.0
MicroEdition-Profile: MIDP-2.0
MIDlet-Icon: $ICO
Created-By: $VEN
!

echo "===> Package binary"

jar cfM bin.jar \
    $(printJarClass $MPS) \
    $(printJarClass $OBJ) \
    $(printJarClass $LIB) \
    -C $OBJ META-INF \
    -C $RES . \
    LICENSE

# http://www.oracle.com/technetwork/systems/index-156899.html

cat > bin.jad <<!
Manifest-Version: 1.0
MIDlet-1: $NAM, $ICO, FW
MIDlet-Name: $NAM
MIDlet-Vendor: $VEN
MIDlet-Jar-URL: $URL
MIDlet-Jar-Size: $(stat -c %s bin.jar)
MIDlet-Version: $VER
MicroEdition-Configuration: CLDC-1.0
MicroEdition-Profile: MIDP-2.0
MIDlet-Icon: $ICO
Created-By: $VEN
!

#proguard -injars libs -outjars classes \
#         `find $LIBDIR -type f -name '*.jar' -printf '-libraryjars %p '`
#         -dontoptimize -dontshrink -microedition -dontobfuscate \
#         -overloadaggressively -repackageclasses '' -allowaccessmodification \
#         -keep public class FW extends javax.microedition.midlet.MIDlet
