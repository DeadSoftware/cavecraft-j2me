class Lib_items_store
 {
  /*Craft*/
  static byte[] [] craftIN_item;
  static byte[] [] craftIN_sum;
  static byte[] craftOUT_item;
  static int[] craftOUT_sum;
  static byte[] craftOUT_flag;

 /*Craft*/
  public static void resetcrafts(int len)
   {
    try {
     craftIN_item = new byte[4][len];
     craftIN_sum = new byte[4][len];
     craftOUT_item = new byte[len];
     craftOUT_sum = new int[len];
     craftOUT_flag = new byte[len];
    } catch(Exception e) {e.printStackTrace();}
   }

  public static void setcraft(int id, int item0, int sum0, int item1, int sum1,  int item2, int sum2, int item3, int sum3, int outitem, int outsum, int flag)
   {
    try {
     craftIN_item[0][id] = (byte) item0;
     craftIN_sum[0][id] = (byte) sum0;
     craftIN_item[1][id] = (byte) item1;
     craftIN_sum[1][id] = (byte) sum1;
     craftIN_item[2][id] = (byte) item2;
     craftIN_sum[2][id] = (byte) sum2;
     craftIN_item[3][id] = (byte) item3;
     craftIN_sum[3][id] = (byte) sum3;
     craftOUT_item[id] = (byte) outitem;
     craftOUT_sum[id] = outsum;
     craftOUT_flag[id] = (byte) flag;
    } catch(Exception e) {e.printStackTrace();}
   }

  public static void setcraftin(int id, int num, int item, int sum)
  {
     try {
       craftIN_item[num][id] = (byte) item;
       craftIN_sum[num][id] = (byte) sum;
     } catch(Exception e) {}
  }

  public static void setcraftout(int id, int item, int sum, int flag)
  {
     try {
       craftOUT_item[id] = (byte) item;
       craftOUT_sum[id] = (byte) item;
       craftOUT_flag[id] = (byte) flag;
     } catch(Exception e) {}
  }

  public static void setcraftinitem(int id, int num, int item)
   {
    try {
     craftIN_item[num][id] = (byte) item;
    } catch(Exception e) {}
   }

  public static void setcraftinsum(int id, int num, int sum)
   {
    try {
     craftIN_sum[num][id] = (byte) sum;
    } catch(Exception e) {}
   }

  public static void setcraftoutitem(int id, int item)
   {
    try {
     craftOUT_item[id] = (byte) item;
    } catch(Exception e) {}
   }

  public static void setcraftoutsum(int id, int sum)
   {
    try {
     craftOUT_sum[id] = sum;
    } catch(Exception e) {}
   }

  public static void setcraftoutflag(int id, int flag)
   {
    try {
     craftOUT_flag[id] = (byte) flag;
    } catch(Exception e) {}
   }

  public static int getcraftinitem(int id, int num)
   {
    try {
     return craftIN_item[num][id] & 0xFF;
    } catch(Exception e) { return 0;}
   }

  public static int getcraftinsum(int id, int num)
   {
    try {
     return craftIN_sum[num][id] & 0xFF;
    } catch(Exception e) { return 0;}
   }

  public static int getcraftoutitem(int id)
   {
    try {
     return craftOUT_item[id] & 0xFF;
    } catch(Exception e) { return 0;}
   }

  public static int getcraftoutsum(int id)
   {
    try {
     return craftOUT_sum[id];
    } catch(Exception e) { return 0;}
   }

  public static int getcraftoutflag(int id)
   {
    try {
     return craftOUT_flag[id] & 0xFF;
    } catch(Exception e) { return 0xFF;}
   }
 }
